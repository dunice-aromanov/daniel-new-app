var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/pax', function(req, res, next) {
  res.render('pax/index', { title: 'Express' });
});

router.get('/res', function(req, res, next) {
  res.render('res/index', { title: 'Express' });
});

router.get('/res-payment', function(req, res, next) {
  res.render('res_payment/index');
});

router.get('/res-pnr', function(req, res, next) {
  res.render('res_pnr/index')
});

router.get('/si', function(req, res, next) {
  res.render('schedule-inventory/index');
});

module.exports = router;
